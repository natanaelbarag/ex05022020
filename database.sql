-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 05, 2020 at 08:56 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `examen_tehnologii`
--

-- --------------------------------------------------------

--
-- Table structure for table `produse`
--

CREATE TABLE `produse` (
  `nume` varchar(40) NOT NULL,
  `categorie` varchar(40) NOT NULL,
  `imagine-url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `produse`
-- 

INSERT INTO `produse` (`nume`, `categorie`, `imagine-url`) VALUES
('one', 'soup', './images/i1.png'),
('two', 'soup', './images/i2.png'),
('three', 'apetizers', './images/i3.png'),
('four', 'apetizers', './images/i4.png'),
('five', 'desserts', './images/i5.png'),
('six', 'desserts', './images/i6.png'),
('seven', 'other', './images/i7.png'),
('logo', 'assets', './images/logo.png'),
('client1', 'clienti', './images/clent1.png'),
('client2', 'clienti', './images/client2.png'),
('client3', 'clienti', './images/client3.png');
